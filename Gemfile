source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

ruby '2.5.1'

gem 'rails', '~> 5.2.0'
gem 'bootstrap', '~> 4.1'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails', '~> 4.3'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1'
gem 'faker', '~> 1.8'
gem 'will_paginate', '~> 3.1'
gem 'bootstrap-will_paginate', '~> 1.0'
gem 'curb', '~> 0.9'
gem 'nokogiri', '~> 1.8'
gem 'colorize', '~> 0.8'
gem 'awesome_print', require: 'ap'
gem 'pg_search', '~> 2.1'
# Haml is required by effective_datatables
gem 'haml-rails', '~> 1.0'
gem 'effective_datatables', '4.0.3'
gem 'effective_bootstrap', '~> 0.0'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
#
# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  # gem 'capybara', '~> 2.13'
  # gem 'selenium-webdriver'
  gem 'table_print'
  gem 'wirble'
  gem 'interactive_editor'
end

group :test do
  gem 'rails-controller-testing', '~> 1.0'
  gem 'minitest-reporters', '~> 1.1'
  gem 'guard', '~> 2.14'
  gem 'guard-minitest', '~> 2.4'
  gem 'sqlite3', '~> 1.3'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'pg', '~> 0.21'
end

group :production do
  gem 'pg', '~> 0.21'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
