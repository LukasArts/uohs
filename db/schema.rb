# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180510203827) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "decision_categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_decision_categories_on_name", unique: true
  end

  create_table "decisions", force: :cascade do |t|
    t.integer "instance", limit: 2
    t.string "matter"
    t.jsonb "participants"
    t.bigint "decision_category_id"
    t.integer "year", limit: 2
    t.date "effective_date"
    t.string "ref_number"
    t.date "publication_date"
    t.integer "url_id"
    t.text "body"
    t.decimal "fitness", precision: 3, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.tsvector "textsearchable_vector"
    t.index "to_tsvector('uohs'::regconfig, participants)", name: "index_decisions_on_participants", using: :gin
    t.index ["decision_category_id"], name: "index_decisions_on_decision_category_id"
    t.index ["effective_date"], name: "index_decisions_on_effective_date", order: { effective_date: :desc }, where: "(effective_date IS NOT NULL)"
    t.index ["instance"], name: "index_decisions_on_instance"
    t.index ["matter"], name: "index_decisions_on_matter"
    t.index ["publication_date"], name: "index_decisions_on_publication_date"
    t.index ["ref_number"], name: "index_decisions_on_ref_number", unique: true, where: "(ref_number IS NOT NULL)"
    t.index ["textsearchable_vector"], name: "index_decisions_on_textsearchable_vector", using: :gin
    t.index ["url_id"], name: "index_decisions_on_url_id", unique: true
    t.index ["year"], name: "index_decisions_on_year", order: { year: :desc }
  end

  create_table "participant_decision_relationships", force: :cascade do |t|
    t.bigint "participant_id"
    t.integer "party", limit: 2
    t.bigint "decision_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["decision_id"], name: "index_participant_decision_relationships_on_decision_id"
    t.index ["participant_id"], name: "index_participant_decision_relationships_on_participant_id"
  end

  create_table "participants", force: :cascade do |t|
    t.string "ico"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ico"], name: "index_participants_on_ico", unique: true
  end

  create_table "related_decisions", id: false, force: :cascade do |t|
    t.bigint "referring_id"
    t.bigint "referenced_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["referenced_id"], name: "index_related_decisions_on_referenced_id"
    t.index ["referring_id", "referenced_id"], name: "index_on_referring_referenced", unique: true, order: { referring_id: :desc, referenced_id: :desc }
    t.index ["referring_id"], name: "index_related_decisions_on_referring_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.boolean "admin"
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.string "reset_digest"
    t.datetime "reset_sent_at"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "decisions", "decision_categories"
  add_foreign_key "participant_decision_relationships", "decisions", on_update: :cascade, on_delete: :nullify
  add_foreign_key "participant_decision_relationships", "participants", on_update: :cascade, on_delete: :nullify
  add_foreign_key "related_decisions", "decisions", column: "referring_id", primary_key: "url_id"
end
