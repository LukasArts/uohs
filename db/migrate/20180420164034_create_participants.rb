class CreateParticipants < ActiveRecord::Migration[5.1]
  def change
    create_table :participants do |t|
      t.string :ico
      t.string :name

      t.timestamps
    end
    add_index :participants, :ico, unique: true
  end
end
