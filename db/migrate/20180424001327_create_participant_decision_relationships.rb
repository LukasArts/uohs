class CreateParticipantDecisionRelationships < ActiveRecord::Migration[5.1]
  def change
    create_table :participant_decision_relationships do |t|
      t.references :participant, { foreign_key: { on_update: :cascade, on_delete: :nullify } }
      t.integer    :party, limit: 1
      t.references :decision, { foreign_key: { on_update: :cascade, on_delete: :nullify } }

      t.timestamps
    end
  end
end
