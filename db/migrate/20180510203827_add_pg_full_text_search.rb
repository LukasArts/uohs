class AddPgFullTextSearch < ActiveRecord::Migration[5.1]

  def change
    ts_config_name = "public.uohs".freeze
    ts_config = <<-TSCONFIG
      CREATE EXTENSION IF NOT EXISTS unaccent;
      DROP TEXT SEARCH CONFIGURATION IF EXISTS #{ts_config_name};
      CREATE TEXT SEARCH CONFIGURATION #{ts_config_name} ( COPY = 'english' );
      ALTER TEXT SEARCH CONFIGURATION #{ts_config_name} 
        ALTER MAPPING FOR asciihword, asciiword, hword, hword_asciipart, hword_part, word 
        WITH unaccent,english_stem;
    TSCONFIG

    ts_column = <<-TSCOLUMN
      UPDATE decisions SET textsearchable_vector = x.textsearchable_vector
      FROM (
        SELECT id,
          setweight(to_tsvector('#{ts_config_name}', COALESCE(matter,'')),'A') ||
          setweight(to_tsvector('#{ts_config_name}', COALESCE(body,'')), 'B')
          AS textsearchable_vector
        FROM decisions
      ) AS x
      WHERE x.id = decisions.id;
    TSCOLUMN

    trigger_function = <<-TRIGGER_FUN
      CREATE FUNCTION decision_weighted_tsv_trigger() RETURNS trigger AS $$
	      begin
          new.textsearchable_vector :=
            setweight(to_tsvector('#{ts_config_name}', COALESCE(new.matter,'')),'A') ||
            setweight(to_tsvector('#{ts_config_name}', COALESCE(new.body,'')), 'B');
          return new;
        end
      $$ LANGUAGE plpgsql;
    TRIGGER_FUN

    trigger = <<-TRIGGER
      CREATE TRIGGER update_textsearchable_vector BEFORE INSERT OR UPDATE
      ON decisions
      FOR EACH ROW EXECUTE PROCEDURE decision_weighted_tsv_trigger();
    TRIGGER

    change_table :decisions do |t|
      t.tsvector :textsearchable_vector
      t.index :textsearchable_vector, { using: :gin }
    end

    reversible do |dir|
      dir.up do
        say_with_time 'Setting up full text search configuration' do
          suppress_messages do
            execute(ts_config)
            execute(ts_column)
            execute(trigger_function)
            execute(trigger)
          end
        end
      end
      dir.down do
        say_with_time 'Dropping full text search configuration' do
          suppress_messages do
            execute "DROP EXTENSION IF EXISTS unaccent CASCADE;"
            execute "DROP TEXT SEARCH CONFIGURATION IF EXISTS #{ts_config_name};"
            execute "DROP FUNCTION IF EXISTS decision_weighted_tsv_trigger() CASCADE;"
           end
        end
      end
    end

    add_index :decisions, "to_tsvector('#{ts_config_name}', participants)", { using: :gin, name: 'index_decisions_on_participants' }

  end

end
