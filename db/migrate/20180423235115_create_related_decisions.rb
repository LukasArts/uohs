class CreateRelatedDecisions < ActiveRecord::Migration[5.1]
  def change
    create_table :related_decisions, id: false do |t|
      t.references :referring, foreign_key: { to_table: :decisions, primary_key: :url_id }
      t.references :referenced
      t.index [:referring_id, :referenced_id], name: :index_on_referring_referenced, unique: true, order: {referring_id: :desc, referenced_id: :desc}

      t.timestamps
    end
  end
end
