class CreateDecisions < ActiveRecord::Migration[5.1]
  def change
    create_table :decisions do |t|
      t.integer :instance, limit: 1, index: true
      t.string :matter, index: true
      t.jsonb :participants
      t.references :decision_category, foreign_key: true
      t.integer :year, limit: 2
      t.date :effective_date
      t.string :ref_number
      t.date :publication_date
      t.integer :url_id, index: { unique: true }
      t.text :body
      t.decimal :fitness, precision: 3, scale: 2, null: true, default: nil

      t.timestamps

      t.index :year, { order: { year: :desc } }
      t.index :effective_date, { where: 'effective_date IS NOT NULL', order: { effective_date: :desc } }
      t.index :ref_number, { where: 'ref_number IS NOT NULL', unique: true }
      t.index :publication_date, { order: { effective_date: :desc } }
    end
  end
end
