class CreateDecisionCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :decision_categories do |t|
      t.string :name, unique: true

      t.timestamps
    end
    add_index :decision_categories, :name, unique: true
  end
end
