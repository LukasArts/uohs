class ModifyDecisions < ActiveRecord::Migration[5.1]
  def change
    trigger = <<-MIGRATION
      CREATE TRIGGER tsv_text_tsearch_update BEFORE INSERT OR UPDATE
      ON decisions FOR EACH ROW EXECUTE PROCEDURE
      tsvector_update_trigger(tsv_text_tsearch, 'public.simple_unaccented', body);
    MIGRATION

    reversible do |dir|
      change_table :decisions do |t|
        dir.up do
          t.change :participants, :jsonb
          execute(trigger)
        end
        dir.down do
          t.change :participants, :json
          execute "DROP TRIGGER IF EXISTS tsv_text_tsearch_update ON decisions;"
        end
      end
    end
    add_column :decisions, :tsv_text_tsearch, :tsvector

    add_index :decisions, :instance
    add_index :decisions, :matter
    add_index :decisions, :participants, using: :gin
    add_index :decisions, :tsv_text_tsearch, using: :gin
    # add_index :decisions, "to_tsvector('simple', matter || ' ' || body)", using: :gin, name: 'decisions_idx'
  end
end
