Rails.application.routes.draw do
  # get 'decision_categories/edit'

  get 'password_resets/new'

  get 'password_resets/edit'

  root   'static_pages#home'
  get    '/help',    to: 'static_pages#help'
  get    '/about',   to: 'static_pages#about'
  get    '/contact', to: 'static_pages#contact'
  get    '/signup',  to: 'users#new'
  post   '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get    '/status',  to: 'synchronization#index'
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :participants,        only: [:index, :show, :create, :destroy]
  resources :decisions,           only: [:index, :show, :create, :destroy]
  resources :decision_categories
  resource  :synchronization, controller: 'synchronization'
end
