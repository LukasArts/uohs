require 'test_helper'

class ParticipantsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user        = users(:lukas)
    @other_user  = users(:badman)
    @participant = participants(:valid)
  end

  test "should redirect index when not logged in" do
    get participants_path
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Participant.count' do
      delete participant_path(@participant)
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@other_user)
    assert_no_difference 'Participant.count' do
      delete participant_path(@participant)
    end
    assert_redirected_to root_url
  end

end
