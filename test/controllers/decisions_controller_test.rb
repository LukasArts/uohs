require 'test_helper'

class DecisionsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @decision   = decisions(:linac)
    @other_user = users(:badman)
  end

  test "should redirect index when not loged in" do
    get decisions_path
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Decision.count' do
      delete decision_path(@decision)
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@other_user)
    assert_no_difference 'Decision.count' do
      delete decision_path(@decision)
    end
    assert_redirected_to root_url
  end

end
