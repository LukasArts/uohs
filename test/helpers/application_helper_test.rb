require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title, "ÚOHS Application"
    assert_equal full_title("Help"), "Help | ÚOHS Application"
  end
end