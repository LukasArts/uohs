require 'test_helper'

class DecisionCategoryTest < ActiveSupport::TestCase

  def setup
    @category = decision_categories(:category_0)
  end

  test "should be valid" do
    assert @category.valid?
  end

  test "name should be present" do
    @category.name = ""
    assert_not @category.valid?
  end

  test "name shoud not be too long" do
    @category.name = "a" * 300
    assert_not @category.valid?
  end

end
