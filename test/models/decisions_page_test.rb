require 'test_helper'

class DecisionsPageTest < ActiveSupport::TestCase

  def setup
    @first  = DecisionsTablePage.new(url: URI.join('file:///', file_fixture(
        'first_decisions_page.html').to_s))
    @second = DecisionsTablePage.new(url: URI.join('file:///', file_fixture(
        'second_decisions_page.html').to_s))
    @last   = DecisionsTablePage.new(url: URI.join('file:///', file_fixture(
        'last_decisions_page.html').to_s))
  end

  test "should collect decision ids from page" do
    @first.decision_ids.each do |id|
      assert_match /\d+/, id, "#{id} should be numeric-only id string"
    end
  end

  test "next_page should be nil only on the last_page" do
    assert_nil @last.next_page
    assert_not_nil @first.next_page
  end


end