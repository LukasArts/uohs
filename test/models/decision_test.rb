require 'test_helper'

class DecisionTest < ActiveSupport::TestCase

  def setup
    @decision = decisions(:linac)
  end

  test "should be valid" do
    assert @decision.valid?
  end

  test "instance should be present" do
    @decision.instance = "   "
    assert_not @decision.valid?
  end

  test "instance should be a positive integer" do
    invalid_instances = %w[1.5 I i II ii 1a a2 11a1 Ⅱ] + [0, -3, 1.2]
    invalid_instances.each do |invalid_instance|
      @decision.instance = invalid_instance
      assert_not @decision.valid?, "#{invalid_instance.inspect} should be invalid"
    end
  end

  test "matter should be present" do
    @decision.matter = "   "
    assert_not @decision.valid?
  end

  test "matter should not be too long" do
    @decision.matter = "a" * 300
    assert_not @decision.valid?
  end

  test "decision should be present" do
    @decision.decision_category_id = nil
    assert_not @decision.valid?
  end

  test "year should be present" do
    @decision.year = ""
    assert_not @decision.valid?
  end

  test "year should be written as 4 digits" do
    assert_match(/\d{4}/,@decision.year.to_s)
  end

  test "url_id should be present" do
    @decision.url_id = "   "
    assert_not @decision.valid?
  end

  test "body should be present" do
    @decision.body = "   "
    assert_not @decision.valid?
  end

end
