require 'test_helper'

class ParticipantTest < ActiveSupport::TestCase

  def setup
    @participant = participants(:valid)
    @invalid_participant = participants(:invalid)
  end

  test "should be valid" do
    assert @participant.valid?
  end

  test "invalid participant should be invalid" do
    assert_not @invalid_participant.valid?
  end

  test "name should be present" do
    @participant.name = "    "
    assert_not @participant.valid?
  end

  test "ico should be present" do
    @participant.ico = "    "
    assert_not @participant.valid?
  end

  test "ico validation should reject invalid icos" do
    invalid_icos = %w[12345678 4444 4753a798 47539798z]
    invalid_icos.each do |invalid_ico|
      @participant.ico = invalid_ico
      assert_not @participant.valid?, "#{invalid_ico.inspect} should be invalid"
    end
  end

  test "icos should be unique" do
    duplicate_participant = @participant.dup
    @participant.save
    assert_not duplicate_participant.valid?
  end

end
