require 'test_helper'

class ParticipantDecisionRelationshipTest < ActiveSupport::TestCase

  def setup
    @relationship = participant_decision_relationships(:one)
  end

  test "should be valid" do
    assert @relationship.valid?
  end

  test "participant should be present" do
    @relationship.participant_id = nil
    assert_not @relationship.valid?
  end

  test "decision should be present" do
    @relationship.decision_id = nil
    assert_not @relationship.valid?
  end

end
