require 'test_helper'

class ParticipantsIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin       = users(:lukas)
    @non_admin   = users(:badman)
    @participant = participants(:valid)
  end

  test "index as admin including pagination and delete links" do
    log_in_as(@admin)
    get participants_path
    assert_template 'participants/index'
    assert_select "div.pagination", count: 2
    first_page_of_participants = Participant.paginate(page: 1)
    first_page_of_participants.each do |participant|
      assert_select 'a[href=?]', participant_path(participant), text: participant.name
      assert_select 'a[href=?]', participant_path(participant), text: 'delete'
    end
    assert_difference 'Participant.count', -1 do
      delete participant_path(@participant)
    end
  end

  test "index as non-admin" do
    log_in_as(@non_admin)
    get users_path
    assert_select 'a', text: 'delete', count: 0
  end

end
