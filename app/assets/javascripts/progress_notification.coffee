# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
App.room = App.cable.subscriptions.create "ProgressNotificationChannel",
  received: (data) ->
    $('#progress-bar').attr('aria-valuenow', data['value']);
    $('#progress-bar').attr('style', 'width:' + data['value'] + '%');
    $('#actual-progress').text(data['value']);