// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// JQuery must come before rails-ujs, otherwise there will be "Can’t verify CSRF token authenticity" exception.
// For Bootstrap, the order is jquery, then popper, then bootstrap.
// For Effective DataTables the order is effective_bootstrap, effective_datatables.
//
//= require jquery3
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require popper
//= require bootstrap
//= require effective_bootstrap
//= require effective_datatables
//= require_tree .