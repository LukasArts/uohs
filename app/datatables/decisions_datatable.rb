class DecisionsDatatable < Effective::Datatable

  filters do
    filter :fulltext, ''
  end

  datatable do
    order :effective_date, :desc

    col :instance, as: :integer, visible: false, search: { as: :select,
                                                           collection: Decision.select(:instance).distinct.
                                                               map { |decision| decision.instance } }
    col :matter do |decision|
      link_to decision.matter, decision
    end
    col :participants do |event|
      tag.ul do
        JSON.parse(event.participants).map { |participant| tag.li participant }.join().html_safe
      end
    end.search do |collection, term, column, sql_column|
      collection = Decision.search_participants(term)
    end
    col :category, visible: true, search: { as: :select, collection: DecisionCategory.all, multiple: true } do |decision|
      decision.category.to_s
    end
    col :effective_date, as: :date, visible: false
    col :year, visible: false
  end

  collection do
    if filters[:fulltext].present?
      Decision.search_text(filters[:fulltext])
    else
      Decision.all
    end
  end


end