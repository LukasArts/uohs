class DecisionsSynchronizationJob < ApplicationJob
  queue_as :default
  around_perform :reset_progress

  rescue_from(StandardError) do |exception|

  end

  def perform(*args)
    Synchronization.new.synchronize
  end


  private

    def reset_progress
      begin
        Redis.current.pipelined do
          Rails.logger.info("===ABRAXAS=== initializing Redis variables before synchronization job".green)
          Redis.current.set(:in_progress, true)
          Redis.current.set(:current_progress, 0)
          Redis.current.expire(:in_progress, 3600)
          Redis.current.expire(:current_progress, 3600)
          Redis.current.del(:updated_ids)
          Redis.current.set(:synchronization_start_time, Time.zone.now)
          Redis.current.del(:page_counter)
          Redis.current.del(:url_ids_not_in_db)
        end
        yield
      ensure
        Rails.logger.info("===ABRAXAS=== clearing Redis variables after synchronization job".green)
        Redis.current.pipelined do
          Redis.current.del(:current_progress)
          Redis.current.set(:in_progress, false)
          Redis.current.set(:synchronization_stop_time, Time.zone.now)
        end
      end
    end

end
