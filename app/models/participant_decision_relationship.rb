class ParticipantDecisionRelationship < ApplicationRecord
  belongs_to :participant
  belongs_to :decision
  validates :participant_id, presence: true
  validates :decision_id,    presence: true
end
