class Participant < ApplicationRecord
  default_scope -> { order(:name) }
  validates :name, presence: true
  validates :ico, presence: true,
                  length: { is: 8 },
                  numericality: { only_integer: true },
                  uniqueness: true
  validate  :mod11_rule

  has_many :participant_decision_relationships
  has_many :decisions, through: :participant_decision_relationships,
                       dependent: :nullify


  private

    # Checks whether check digit of Subject Identification Number (IČO) is valid.
    def Participant.valid_ico?(ico)
      ApplicationController.helpers.ico_check_digit(ico) == ico.last
    end

    # Confirms that IČO is valid according to mod 11 rule.
    def mod11_rule
      unless Participant.valid_ico? ico
        errors.add(:ico, "should be valid")
      end
    end

end
