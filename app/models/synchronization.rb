class Synchronization
  include ActiveModel::Model

  attr_reader :decision_count

  validates :decision_count, presence: true, numericality: true

  def initialize(attributes = {})
    super
    @first_page     ||= first_page
    /Nalezen. (?<resolutions_count>\d*) rozhodnutí/ =~ @first_page.at_css('#text_l > p > strong').text
    @decision_count ||= resolutions_count
  end

  def synchronize
    # Try to synchronize previously failed decision IDs first.
    failed_ids = Redis.current.smembers(:failed_ids)
    download_decisions(failed_ids) unless failed_ids.empty?

    first_page_url      = PUBLIC_PROCUREMENT_URL + '/sbirky-rozhodnuti.html'
    decisions_page      = DecisionsTablePage.new(url: first_page_url)
    decisions_published = decisions_page.count.to_f
    decisions_in_db     = Decision.count
    decisions_processed = 0.0
    decisions_to_dl     = decisions_published - decisions_in_db

    Rails.logger.info { "===ABRAXAS=== About to download #{decisions_to_dl} decisions.".bold.green +
        " There are #{decisions_in_db} in the db and #{Redis.current.scard(:failed_ids)} on the failed_ids list}" }

    begin
      Redis.current.incr(:page_counter)
      url_ids = decisions_page.decision_ids
      url_ids.delete_if { |url_id| Decision.find_by(url_id: url_id) }
      current_page_no = decisions_page.page.css('div#paging span').text
      last_page_url = decisions_page.page.css('div#paging a').last[:href]
      last_page_no1 = /(?<last_page>\d+)\.html$/.match(last_page_url)[:last_page].to_i
      last_page_no2 = (@decision_count.to_f/10r).ceil
      ap "at page #{current_page_no} (redis: #{Redis.current.get(:page_counter)}, last_page_no_1 #{last_page_no1}, last_page_no_2 #{last_page_no2}"
      Rails.logger.error { "===ABRAXAS=== scraping went fubar - last_page_no #{last_page_no1.to_s.bold.red} != #{last_page_no2.to_s.bold.red} @ page #{current_page_no}" } if last_page_no1 != last_page_no2

      unless url_ids.empty?
        Redis.current.sadd(:url_ids_not_in_db, url_ids)
        download_decisions(url_ids)
        decisions_processed += url_ids.count
        update_progress(decisions_processed / decisions_to_dl)
      end

     # end while decisions_in_db + url_ids.count < decisions_published
    end while decisions_page.load_page(decisions_page.next_page) && decisions_processed < decisions_to_dl

    Rails.logger.info { "===ABRAXAS=== Finished synchronize run".bold.green +
                        " with #{Redis.current.scard(:updated_ids)} decisions on updated_ids list}" +
                        " and #{Redis.current.scard(:failed_ids)} decisions on failed_ids list}" }
  end


  private


    BASE_URL = 'https://www.uohs.cz'.freeze
    PUBLIC_PROCUREMENT_URL = (BASE_URL + '/cs/verejne-zakazky/sbirky-rozhodnuti').freeze


    # Downloads decisions specified by their URL ids and inserts them into db.
    def download_decisions(ids)
      log_level = Rails.logger.level
      instance_map  = { 'I.' => 1, 'II.' => 2, 'i.' => 1, 'ii.' => 2 }
      easy_options  = { :follow_location => true, verbose: Rails.env.development? ? true : false }
      multi_options = { :pipeline => Curl::CURLPIPE_HTTP1 }
      id_pattern = /detail-(?<id>\d+)/

      urls = ids.map {|id| PUBLIC_PROCUREMENT_URL + "/detail-#{id}.html"}
      Curl::Multi.get(urls, easy_options, multi_options) do |easy|
        doc     = Nokogiri::HTML(easy.body_str)
        details = details_from_document(doc)
        url_id  = id_pattern.match(easy.last_effective_url)[:id]

        begin
          ap details
          dk = DecisionCategory.find_or_create_by!(name: details[:'Typ rozhodnutí'] * "") unless details[:'Typ rozhodnutí'].empty?
          *_dummy, effective_date = details[:'Datum nabytí právní moci']

          Rails.logger.level = :info
          decision = Decision.new(instance: instance_map[details[:Instance] * ""].to_i,
                                  matter: details[:Věc] * "",
                                  participants: details[:Účastníci].to_json,
                                  decision_category_id: dk.nil? ? nil : dk[:id],
                                  year: details[:Rok] * "",
                                  effective_date: effective_date,
                                  url_id: url_id,
                                  body: doc.at_css('div.res_text').to_s)
          decision[:fitness] = 0 if decision.invalid?
          decision.save(validate: false)
          Rails.logger.level = log_level

          details[:'Související rozhodnutí'].each do |related_url|
            url_id = id_pattern.match(related_url)[:id]
            begin
              rd = RelatedDecision.new(referring: decision, referenced: Decision.new(url_id: url_id))
              rd.save #(validate: false)
            rescue StandardError => error
              Rails.logger.error {"===ABRAXAS error in download_decisions: #{error.inspect}" + " for #{easy.last_effective_url}" }
              ap error.backtrace
            end
          end

          Rails.logger.info {"===ABRAXAS=== decision with url_id: #{url_id.cyan} inserted into the db"}
          Redis.current.pipelined do
            Redis.current.sadd(:updated_ids, url_id)
            Redis.current.srem(:failed_ids, url_id)
          end

        rescue ActiveRecord::RecordInvalid => invalid
          Rails.logger.warn {'===ABRAXAS=== Decision.create!'.bold.red + " for #{easy.last_effective_url} " +
              "#{invalid.message}. Values: #{invalid.instance_values}"}
          Redis.current.sadd(:failed_ids, url_id)
          ap invalid.backtrace
        rescue ActiveRecord::RecordNotUnique => invalid
          Rails.logger.warn {'===ABRAXAS=== Decision.create!'.bold.red + " for #{easy.last_effective_url} " +
              "failed with #{invalid.message}. Values: #{invalid.instance_values}"}
          ap invalid.backtrace
        rescue StandardError => error
          Rails.logger.error {"===ABRAXAS error in download_decisions: #{error.inspect}" + " for #{easy.last_effective_url}" }
          ap error.backtrace
        ensure
          Rails.logger.level = log_level
        end
      end

    end


    # Constructs Decision URL on the ÚOHS web for the given url_id.
    def decision_url_from_id(id)
      PUBLIC_PROCUREMENT_URL + "/detail-#{id}.html"
    end


    # Gets first page with decisions.
    def first_page
      Nokogiri::HTML(
          Curl.post(PUBLIC_PROCUREMENT_URL + '.html', { odbor: 2 }) do |easy|
            easy.enable_cookies = true
            easy.verbose        = true if Rails.env.development?
          end.body_str)
    end


    # Updates the progressbar.
    def update_progress(progress)
      rounded_progress = '%.1f' % (progress * 100)

      if rounded_progress != Redis.current.get(:current_progress)
        Redis.current.set(:current_progress, rounded_progress)
        ActionCable.server.broadcast('progress_notification_channel', value: rounded_progress)
      end
    end


    # Constructs URL of the decision for the given decision id.
    def decision_url(decision_id)
      PUBLIC_PROCUREMENT_URL + "/detail-#{decision_id}.html"
    end


    # Parse resolution details table into a hash.
    #
    # The data from the table are treated as simple key => value pairs with value being an Array
    # of mostly one, but possibly many (e.g. "Účastnící" field) values.
    # Additional data are extracted from the text and appended to the resulting Hash.
    #
    # @param [Nokogiri::HTML::Document] web page containing the resolution
    # @return [Hash] details of the resolution extracted from the table#resolution_detail element
    #   (e.g. {:Instance=>["I."], :Věc=>["Porušení…"], :Účastníci=>[""]…})
    def details_from_document(doc)
      values = Hash.new do |hash,key|
        value = []
        hash[key] = value
      end
      date_pattern = /^\d{1,2}\.[[:blank:]]*\d{1,2}.[[:blank:]]*\d{4}$/

      details = doc.css('table#resolution_detail tr').each_with_object(values) do |row, hsh|
        hsh[row.at_css('th').text.strip.gsub(/^"|"$/, '').to_sym] = row.css('td').each_with_object([]) do |cell, ary|
          if !cell.css('a').empty?
            ary.concat (cell.css('a').each_with_object([]) { |link, memo| memo << link['href'] })
          elsif !cell.css('li').empty?
            ary.concat cell.css('li').map(&:text)
          elsif date_pattern.match?(cell.text)
            ary << Date.parse(cell.text)
          else
            ary.concat unquote(cell.text.tr("\r\t", '').strip.split("\n"))
          end
        end
      end

      details
    end


    # Removes quotes around the quoted string.
    def unquote(str)
      s = str.dup

      case str[0,1]
      when "'", '"', '`'
        s[0] = ''
      end

      case str[-1,1]
      when "'", '"', '`'
        s[-1] = ''
      end

      return s
    end


end