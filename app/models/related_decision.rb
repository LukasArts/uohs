class RelatedDecision < ApplicationRecord
  belongs_to :referring,  class_name: 'Decision', primary_key: :url_id
  belongs_to :referenced, class_name: 'Decision', primary_key: :url_id
  validates :referring_id,  presence: true
  validates :referenced_id, presence: true
end
