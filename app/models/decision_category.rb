class DecisionCategory < ApplicationRecord
  has_one :decision
  default_scope -> { order(:name) }
  validates :name, presence: true,
                   length: { maximum: 255 },
                   uniqueness: { case_sensitive: false }

  def to_s
    name
  end

end
