class Decision < ApplicationRecord
  include PgSearch

  pg_search_scope :search_text, against: %i(matter body),
                                        using: {
                                            tsearch: {
                                                tsvector_column: :textsearchable_vector,
                                                negation: true,
                                                prefix: true,
                                                dictionary: 'public.uohs'
                                            }
                                        }
  pg_search_scope :search_participants, against: :participants,
                                        using: {
                                            tsearch: {
                                                normalization: 4,
                                                negation: true,
                                                prefix: true,
                                                dictionary: 'public.uohs'
                                            }
                                        }

  belongs_to :category, class_name: "DecisionCategory",
                        foreign_key: "decision_category_id"
  has_many :related_decisions, foreign_key: :referring_id,
                               primary_key: :url_id
  has_many :related, through: :related_decisions,
                     source: :referenced

  validates :instance, presence: true,
                       numericality: { only_integer: true,
                                       greater_than: 0 }
  validates :matter, presence: true,
                     length: { maximum: 255 }
  validates :decision_category_id, presence: true, on: :update
  validates :year, presence: true,
                   numericality: { only_integer: true },
                   length: { is: 4 }
  validates :effective_date, presence: true
  validates :ref_number, presence: true, on: :update
  validates :publication_date, presence: true, on: :update
  validates :url_id, presence: true,
                     uniqueness: true
  validates :body, presence: true
end
