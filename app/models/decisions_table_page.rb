class DecisionsTablePage
  include ActiveModel::Model

  attr_accessor :url
  attr_reader :page


  def initialize(attributes = {})
    super
    load_page(attributes[:url])
  end


  # Collects numeric part of decision URLs in the given page.
  def decision_ids
    row_selector  = 'table#resolutions_list tr:has(td)'
    link_selector = 'td.al a'
    pattern       = /-(?<url_id>\d*)\.html/

    page.css(row_selector).each_with_object([]) do |row, ary|
      url_id = pattern.match(row.at_css(link_selector)['href'])[:url_id]
      ary << url_id
    end
  end


  # Returns next decisions page.
  def next_page
    next_page_selector = 'div#paging > span + a'
    absolutize(@page.at_css(next_page_selector)[:href]) unless @page.at_css(next_page_selector).nil?
  end


  # Loads page from the URL.
  def load_page(url)
    @page = Nokogiri::HTML(Curl.get(url).body_str) unless url.nil?
  end


  # Returns a total number of decisions in Public Procurements section.
  # Works for the first page only.
  def count
    pattern  = /Nalezen. (?<number>\d*) rozhodnutí/
    selector = '#text_l > p > strong'

    count = pattern.match(page.at_css(selector).text)[:number]
    count.nil? ? nil : count.to_i
  end


  def absolutize(url)
    url = 'https://www.uohs.cz' + url unless /^http/.match url
    url
  end


end