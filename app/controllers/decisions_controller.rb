class DecisionsController < ApplicationController
  protect_from_forgery unless: -> { request.format.json? }
  before_action :logged_in_user, only: [:index, :destroy]
  before_action :admin_user,     only: :destroy

  def index
    @datatable = DecisionsDatatable.new
  end

  def show
    @decision = Decision.find(params[:id])
  end

  def destroy
    Decision.find(params[:id]).destroy
    flash[:success] = "Decision deleted"
    redirect_to decisions_url
  end


  private

    def decision_parameters
      params.require(:decision).permit(:instance, :matter, :decision_category_id, :year, :ref_number, :publication_date, :effective_date, :ref_number, :url_id, :body)
    end

end
