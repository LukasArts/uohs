class SynchronizationController < ApplicationController
  before_action :logged_in_user, :admin_user

  def index
    @status ||= Synchronization.new
  end

  def update
    @status = DecisionsSynchronizationJob.perform_later
    # @status = Synchronization.new
    # @new_links = @status.populate_db
  end

end
