class ParticipantsController < ApplicationController
  protect_from_forgery unless: -> { request.format.json? }
  before_action :logged_in_user, only: [:index, :destroy]
  before_action :admin_user,     only: :destroy

  def index
    @participants = Participant.all.paginate(page: params[:page])
  end

  def show
    @participant = Participant.find(params[:id])
    respond_to do |format|
      format.html
      format.json { render json: @participant }
    end
  end

  def create
    participant = Participant.find_by(ico: participant_params[:ico])
    head :conflict, location:participant_path(participant) and return unless participant.nil?

    participant = Participant.new(participant_params)
    if participant.save
      head :created, location: participant_path(participant)
    else
      head :unprocessable_entity
    end

  end

  def destroy
    Participant.find(params[:id]).destroy
    flash[:success] = "Participant deleted"
    redirect_to participants_url
  end


  private

    def participant_params
      params.require(:participant).permit(:ico, :name)
    end

end
