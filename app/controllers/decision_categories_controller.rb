class DecisionCategoriesController < ApplicationController
  protect_from_forgery unless: -> { request.format.json? }
  before_action :logged_in_user, only: [:index, :destroy]
  before_action :admin_user,     only: :destroy

  def index
    @decision_categories = DecisionCategory.all.paginate(page: params[:page])
  end

  def show
  end

  def edit
    @decision_category = DecisionCategory.find(params[:id])
  end

  def update
    @decision_category = DecisionCategory.find(params[:id])
    if @decision_category.update_attributes(decision_category_params)
      flash[:success] = "Category updated"
      redirect_to decision_categories_url
    else
      render 'edit'
    end
  end


  private


  def decision_category_params
    params.require(:decision_category).permit(:name)
  end

end
