module ApplicationHelper

  # Returns the full title on per-page basis.
  def full_title(page_title = '')
    base_title = "ÚOHS Application"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  # Sends progress update to client using WebSockets.
  def progress_bar_notification(percent, message = nil)
    ActionCable.server.broadcast('progress_notification_channel', value: percent, message: message)
  end

  # Non-throwing Redis wrapper for views.
  def redis(cmd = {})
    begin
      Redis.current.send(cmd.keys[0].to_sym, cmd.values[0].to_sym)
    rescue StandardError => e
      ap e.inspect
    end
  end

  # Returns true if given action on given controller is active.
  ACTIVE_CLASS = 'active'.freeze

  def active_for(options)
    name_of_controller = options.fetch(:controller) { nil }
    name_of_action     = options.fetch(:action) { nil }
    request_path       = options.fetch(:path) { nil }

    return ACTIVE_CLASS if request_path && request_path == request.path

    if name_of_controller == controller_name
      ACTIVE_CLASS if name_of_action.nil? || (name_of_action == action_name)
    end
  end

end
