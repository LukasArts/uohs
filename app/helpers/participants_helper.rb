module ParticipantsHelper

  # Returns the check digit for the given IČO.
  def ico_check_digit(ico)
    checksum = ico[0..6].
        reverse.
        chars.
        map(&:to_i).
        to_enum.
        with_index(2).
        inject(0) { |memo,e| memo + e[0] * e[1] }

    ((11 - checksum % 11) % 10).to_s
  end

end
